package ru.tsc.karbainova.tm.api.repository;


import ru.tsc.karbainova.tm.endpoint.AbstractDTOEntity;

import java.util.List;

public interface IRepository<E extends AbstractDTOEntity> {

    List<E> findAll();

    void clear();

    void addAll(List<E> entities);

    void remove(E entity);

    E add(final E entity);


}
