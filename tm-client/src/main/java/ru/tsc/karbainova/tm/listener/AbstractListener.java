package ru.tsc.karbainova.tm.listener;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import ru.tsc.karbainova.tm.api.service.IPropertyService;
import ru.tsc.karbainova.tm.endpoint.AdminUserEndpoint;
import ru.tsc.karbainova.tm.endpoint.Role;
import ru.tsc.karbainova.tm.endpoint.SessionEndpoint;
import ru.tsc.karbainova.tm.endpoint.UserEndpoint;
import ru.tsc.karbainova.tm.event.ConsoleEvent;
import ru.tsc.karbainova.tm.service.SessionService;


public abstract class AbstractListener {
    @NotNull
    @Autowired
    protected IPropertyService propertyService;

    @NotNull
    @Autowired
    protected AdminUserEndpoint adminEndpoint;

    @NotNull
    @Autowired
    protected UserEndpoint userEndpoint;

    @NotNull
    @Autowired
    protected SessionEndpoint sessionEndpoint;

    @NotNull
    @Autowired
    protected SessionService sessionService;

    @Nullable
    public abstract String name();

    @Nullable
    public abstract String description();

    public abstract void handler(@NotNull ConsoleEvent event);

    @Nullable
    public Role[] roles() {
        return null;
    }

    @Override
    @NotNull
    public String toString() {
        @NotNull String result = "";
        @Nullable final String name = name();
        @Nullable final String description = description();
        if (name != null && !name.isEmpty()) result += name + " ";
        if (description != null && !description.isEmpty()) result += ": " + description;
        return result;
    }


}
