package ru.tsc.karbainova.tm.listener.task;

import org.jetbrains.annotations.NotNull;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.tsc.karbainova.tm.event.ConsoleEvent;
import ru.tsc.karbainova.tm.listener.AbstractListenerProject;
import ru.tsc.karbainova.tm.listener.TerminalUtil;

@Component
public class TaskBindByIdProjectListener extends AbstractListenerProject {
    @Override
    @NotNull
    public String name() {
        return "task-bind-by-id-project";
    }

    @Override
    public String arg() {
        return null;
    }

    @Override
    @NotNull
    public String description() {
        return "Bind task to project by id.";
    }

    @Override
    @EventListener(condition = "@taskBindByIdProject.name() == #event.name")
    public void handler(@NotNull ConsoleEvent event) {
        System.out.print("Enter project id: ");
        @NotNull final String projectId = TerminalUtil.nextLine();
        System.out.print("Enter task id: ");
        @NotNull final String taskId = TerminalUtil.nextLine();
        projectEndpoint.taskBindByIdProject(sessionService.getSession(), projectId, taskId);
    }

}
