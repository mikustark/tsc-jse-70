package ru.tsc.karbainova.tm.api.service;

import ru.tsc.karbainova.tm.api.repository.IRepository;
import ru.tsc.karbainova.tm.endpoint.AbstractDTOEntity;

import java.util.List;

public interface IService<E extends AbstractDTOEntity> extends IRepository<E> {

    void addAll(List<E> entities);

    void clear();

    void remove(E entity);
}
