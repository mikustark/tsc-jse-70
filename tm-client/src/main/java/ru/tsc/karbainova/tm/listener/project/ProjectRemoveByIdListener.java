package ru.tsc.karbainova.tm.listener.project;

import org.jetbrains.annotations.NotNull;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.tsc.karbainova.tm.event.ConsoleEvent;
import ru.tsc.karbainova.tm.listener.AbstractListenerProject;
import ru.tsc.karbainova.tm.listener.TerminalUtil;

@Component
public class ProjectRemoveByIdListener extends AbstractListenerProject {
    @Override
    public String name() {
        return "remove-by-id-project";
    }

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String description() {
        return "Remove by index";
    }

    @Override
    @EventListener(condition = "@projectRemoveById.name() == #event.name")
    public void handler(@NotNull ConsoleEvent event) {
        System.out.println("Enter project id: ");
        final String projectId = TerminalUtil.nextLine();
        projectEndpoint.removeProjectById(sessionService.getSession(), projectId);
    }

}
