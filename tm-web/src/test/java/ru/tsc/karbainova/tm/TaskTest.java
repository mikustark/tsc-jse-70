package ru.tsc.karbainova.tm;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;
import ru.tsc.karbainova.tm.api.endpoint.ITaskEndpoint;
import ru.tsc.karbainova.tm.client.TaskFeignClient;
import ru.tsc.karbainova.tm.marker.RestCategory;
import ru.tsc.karbainova.tm.model.Result;
import ru.tsc.karbainova.tm.model.Task;

import java.util.ArrayList;
import java.util.List;

public class TaskTest {

    final Task task1 = new Task("test task 1");

    final Task task2 = new Task("test task 2");

    @Nullable
    private static String sessionId = null;

    @NotNull
    private static final String TASK_URL = "http://localhost:8080/api/tasks/";

    @BeforeClass
    public static void beforeClass() {
        @NotNull final RestTemplate restTemplate = new RestTemplate();
        @NotNull final String url = "http://localhost:8080/auth/login?username=user&password=user";
        @NotNull final ResponseEntity<Result> response = restTemplate.getForEntity(url, Result.class);
        Assert.assertEquals(200, response.getStatusCodeValue());
        Assert.assertNotNull(response.getBody());
        Assert.assertTrue(response.getBody().getSuccess());
        HttpHeaders headersResponse = response.getHeaders();
        List<java.net.HttpCookie> cookies = java.net.HttpCookie.parse(headersResponse.getFirst(HttpHeaders.SET_COOKIE));
        sessionId = cookies.stream()
                .filter(
                        item -> "JSESSIONID".equals(item.getName())
                ).findFirst().get().getValue();
        Assert.assertNotNull(sessionId);
    }

    private static HttpHeaders getHeader() {
        @NotNull final HttpHeaders headers = new HttpHeaders();
        List<String> cookies = new ArrayList<>();
        cookies.add("JSESSIONID" + "=" + sessionId);
        headers.put(HttpHeaders.COOKIE, cookies);
        headers.setContentType(MediaType.APPLICATION_JSON);
        return headers;
    }

    @Before
    public void before() {
        create(task1);
    }

    @Test
    @Category(RestCategory.class)
    public void findTest() {
        Assert.assertEquals(task1.getName(), find(task1.getId()).getName());
    }

    @Nullable
    private Task find(String taskId) {
        @NotNull final RestTemplate restTemplate = new RestTemplate();
        @NotNull final HttpEntity<Task> httpEntity =
                new HttpEntity<>(getHeader());
        @NotNull String url = TASK_URL + "find/" + taskId;
        @NotNull final ResponseEntity<Task> response = restTemplate.exchange(
                url, HttpMethod.GET, httpEntity, Task.class
        );
        Assert.assertEquals(response.getStatusCode(), (HttpStatus.OK));
        return response.getBody();
    }

    @Test
    @Category(RestCategory.class)
    public void createTest() {
        create(task1);
        @Nullable final Task taskNew = find(task1.getId());
        Assert.assertNotNull(taskNew);
        Assert.assertEquals(task1.getId(), taskNew.getId());
    }

    @NotNull
    private void create(Task task) {
        @NotNull String url = TASK_URL + "create";
        @NotNull final RestTemplate restTemplate = new RestTemplate();
        @NotNull final HttpEntity<Task> httpEntity =
                new HttpEntity<>(task, getHeader());
        restTemplate.postForEntity(url, httpEntity, Task.class);
    }

    @Test
    @Category(RestCategory.class)
    public void update() {
        final Task updatedTask = find(task1.getId());
        updatedTask.setName("updated");
        @NotNull final RestTemplate restTemplate = new RestTemplate();
        @NotNull final String url = TASK_URL + "save";
        @NotNull final HttpEntity<Task> httpEntity =
                new HttpEntity<>(updatedTask, getHeader());
        restTemplate.exchange(
                url, HttpMethod.PUT, httpEntity, Task.class
        );
        Assert.assertEquals("updated", find(task1.getId()).getName());
    }

    @Test
    @Category(RestCategory.class)
    public void deleteTest() {
        delete(task1.getId());
        Assert.assertNull(find(task1.getId()));
    }

    private void delete(String taskId) {
        @NotNull final RestTemplate restTemplate = new RestTemplate();
        @NotNull final String url =
                TASK_URL + "delete/" + taskId;
        @NotNull final HttpEntity<Task> httpEntity =
                new HttpEntity<>(getHeader());
        restTemplate.exchange(
                url, HttpMethod.DELETE, httpEntity, Task.class
        );
    }

    @Test
    @Category(RestCategory.class)
    public void findAllTest() {
        Assert.assertEquals(1, findAll().size());
        create(task2);
        Assert.assertEquals(2, findAll().size());
    }

    @NotNull
    private List<Task> findAll() {
        @NotNull final RestTemplate restTemplate = new RestTemplate();
        @NotNull final String url = TASK_URL + "findAll";
        HttpEntity<String> httpEntity = new HttpEntity(getHeader());
        @NotNull final ResponseEntity<List<Task>> response
                = restTemplate.exchange(
                url,
                HttpMethod.GET,
                httpEntity,
                new ParameterizedTypeReference<List<Task>>() {
                }
        );
        Assert.assertEquals(response.getStatusCode(), (HttpStatus.OK));
        Assert.assertNotNull(response.getBody());
        return response.getBody();
    }
}
