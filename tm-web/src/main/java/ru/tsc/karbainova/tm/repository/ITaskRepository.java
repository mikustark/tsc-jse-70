package ru.tsc.karbainova.tm.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import ru.tsc.karbainova.tm.model.Task;

import java.util.List;

public interface ITaskRepository extends JpaRepository<Task, String> {
    @Query("SELECT e FROM Task e WHERE e.userId = :userId AND e.projectId = :projectId")
    List<Task> findAllTaskByProjectId(final String userId, final String projectId);

    @Modifying
    @Query("DELETE FROM Task e WHERE e.userId = :userId AND e.projectId = :projectId")
    void removeAllTaskByProjectId(final String userId, final String projectId);

    @Modifying
    @Query("UPDATE Task e SET e.projectId = :projectId WHERE e.userId = :userId AND e.id = :id")
    void bindTaskToProjectById(final String userId, final String taskId, final String projectId);

    @Modifying
    @Query("UPDATE Task e SET e.projectId = NULL WHERE e.userId = :userId AND e.id = :id")
    void unbindTaskById(final String userId, final String id);

    Task findByUserIdAndId(final String userId, final String id);

    void deleteByUserId(final String userId);

    void deleteByUserIdAndId(final String userId, final String id);

    List<Task> findAllByUserId(final String userId);

    Task findByUserIdAndName(final String userId, final String name);

    @Query("SELECT e FROM Task e WHERE e.userId = :userId")
    Task findByIndexAndUserId(final String userId, final int index);

    void deleteByUserIdAndName(final String userId, final String name);
}
