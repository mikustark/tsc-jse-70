package ru.tsc.karbainova.tm.api.service;

import java.util.List;

public interface IEntityService<E> {
    List<E> findAll();

    E add(final E entity);

    E findById(final String id);

    void clear();

    void removeById(final String id);

    void remove(final E entity);
}
