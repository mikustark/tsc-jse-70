package ru.tsc.karbainova.tm.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.annotation.Secured;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.ModelAndView;
import ru.tsc.karbainova.tm.api.service.IProjectService;
import ru.tsc.karbainova.tm.api.service.ITaskService;
import ru.tsc.karbainova.tm.enumerated.Status;
import ru.tsc.karbainova.tm.model.CustomUser;
import ru.tsc.karbainova.tm.model.Project;
import ru.tsc.karbainova.tm.model.Task;

import java.util.List;

@Controller
public class TaskController {

    @Autowired
    private ITaskService taskService;

    @Autowired
    private IProjectService projectService;

    @Secured({"ROLE_USER", "ROLE_ADMIN"})
    @GetMapping("/task/create")
    public String create(@AuthenticationPrincipal(errorOnInvalidType = true) final CustomUser user) {
        taskService.create(user.getUserId());
        return "redirect:/tasks";
    }

    @Secured({"ROLE_USER", "ROLE_ADMIN"})
    @GetMapping("/task/delete/{id}")
    public String delete(
            @AuthenticationPrincipal(errorOnInvalidType = true) final CustomUser user,
            @PathVariable("id") String id
    ) {
        taskService.removeById(user.getUserId(),id);
        return "redirect:/tasks";
    }

    @Secured({"ROLE_USER", "ROLE_ADMIN"})
    @GetMapping("/task/edit/{id}")
    public ModelAndView edit(
            @AuthenticationPrincipal(errorOnInvalidType = true) final CustomUser user,
            @PathVariable("id") String id
    ) {
        return new ModelAndView(
                "task-create",
                "task", taskService.findById(user.getUserId(),id)
        );
    }

    @Secured({"ROLE_USER", "ROLE_ADMIN"})
    @PostMapping("/task/edit")
    public String edit(
            @AuthenticationPrincipal(errorOnInvalidType = true) final CustomUser user,
            @ModelAttribute("task") Task task
    ) {
        taskService.save(user.getUserId(),task);
        return "redirect:/tasks";
    }

    @ModelAttribute("statuses")
    public Status[] getStatuses() {
        return Status.values();
    }

    @ModelAttribute("viewName")
    public String getViewName() {
        return "Task edit";
    }

    @ModelAttribute("projects")
    public List<Project> getProjects() {
        return projectService.findAll();
    }
}
