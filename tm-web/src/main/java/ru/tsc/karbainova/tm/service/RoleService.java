package ru.tsc.karbainova.tm.service;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.tsc.karbainova.tm.api.service.IRoleService;
import ru.tsc.karbainova.tm.exception.EmptyException;
import ru.tsc.karbainova.tm.model.Role;
import ru.tsc.karbainova.tm.repository.IRoleRepository;

import java.util.Collection;
import java.util.List;
import java.util.Optional;

@Service
@Transactional
public class RoleService extends AbstractEntityService<Role> implements IRoleService {
    @NotNull
    @Autowired
    private IRoleRepository repository;

    @NotNull
    @Override
    @SneakyThrows
    public List<Role> findAll() {
        return repository.findAll();
    }

    @SneakyThrows
    public void addAll(@Nullable final Collection<Role> collection) {
        if (collection == null) return;
        for (Role item : collection) {
            add(item);
        }
    }

    @Nullable
    @Override
    @SneakyThrows
    public Role add(@Nullable final Role entity) {
        if (entity == null) return null;
        repository.save(entity);
        return entity;
    }

    @Nullable
    @Override
    @SneakyThrows
    public Role findById(@Nullable final String id) {
        @NotNull final Optional<String> optionalId = Optional.ofNullable(id);
        return repository.findById(optionalId.orElseThrow(EmptyException::new)).orElse(null);
    }

    @Override
    @SneakyThrows
    public void clear() {
        repository.deleteAll();
    }

    @Override
    @SneakyThrows
    public void removeById(@Nullable final String id) {
        @NotNull final Optional<String> optionalId = Optional.ofNullable(id);
        repository.deleteById(optionalId.orElseThrow(EmptyException::new));
    }

    @Override
    @SneakyThrows
    public void remove(@Nullable final Role entity) {
        if (entity == null) return;
        repository.deleteById(entity.getId());
    }

    @Override
    @NotNull
    @SneakyThrows
    public List<Role> findAllByUserId(@NotNull final String userId) {
        return repository.findAllByUserId(userId);
    }

    @Override
    @SneakyThrows
    public void clear(@NotNull final String userId) {
        repository.deleteByUserId(userId);
    }
}
