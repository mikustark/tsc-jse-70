package ru.tsc.karbainova.tm.client;

import feign.Feign;
import okhttp3.JavaNetCookieJar;
import org.springframework.beans.factory.ObjectFactory;
import org.springframework.boot.autoconfigure.web.HttpMessageConverters;
import org.springframework.cloud.netflix.feign.support.SpringDecoder;
import org.springframework.cloud.netflix.feign.support.SpringEncoder;
import org.springframework.cloud.netflix.feign.support.SpringMvcContract;
import org.springframework.http.converter.FormHttpMessageConverter;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import ru.tsc.karbainova.tm.api.endpoint.IProjectEndpoint;
import ru.tsc.karbainova.tm.model.Project;

import java.net.CookieManager;
import java.net.CookiePolicy;
import java.util.List;

public interface ProjectFeignClient extends IProjectEndpoint {
    String URL = "http://localhost:8080/api/projects";

    static ProjectFeignClient client() {
        final FormHttpMessageConverter converter = new FormHttpMessageConverter();
        final HttpMessageConverters converters = new HttpMessageConverters(converter);
        final ObjectFactory<HttpMessageConverters> objectFactory = () -> converters;
        final CookieManager cookieManager = new CookieManager();
        cookieManager.setCookiePolicy(CookiePolicy.ACCEPT_ALL);
        final okhttp3.OkHttpClient.Builder builder = new okhttp3.OkHttpClient().newBuilder();
        builder.cookieJar(new JavaNetCookieJar(cookieManager));
        return Feign.builder()
                .contract(new SpringMvcContract())
                .encoder(new SpringEncoder(objectFactory))
                .decoder(new SpringDecoder(objectFactory))
                .target(ProjectFeignClient.class, URL);
    }

    @GetMapping("/findAll")
    List<Project> findAll();

    @GetMapping("/find/{id}")
    Project find(@PathVariable("id") final String id);

    @PostMapping("/create")
    Project create(@RequestBody final Project project);

    @PutMapping("/save")
    Project save(@RequestBody final Project project);

    @DeleteMapping("/delete/{id}")
    void delete(@PathVariable("id") final String id);
}
