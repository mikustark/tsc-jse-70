package ru.tsc.karbainova.tm.service;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.tsc.karbainova.tm.api.service.ITaskService;
import ru.tsc.karbainova.tm.exception.EmptyException;
import ru.tsc.karbainova.tm.model.Task;
import ru.tsc.karbainova.tm.repository.ITaskRepository;

import java.util.Collection;
import java.util.List;
import java.util.Optional;

@Service
@Transactional
public class TaskService implements ITaskService {

    @Autowired
    private ITaskRepository repository;

    @Override
    public List<Task> findAll() {
        return repository.findAll();
    }

    @NotNull
    @Override
    @SneakyThrows
    public List<Task> findAll(@NotNull final String userId) {
        return repository.findAllByUserId(userId);
    }

    @Override
    public void addAll(final Collection<Task> collection) {
        if (collection == null) return;
        for (Task item : collection) {
            save(item);
        }
    }

    @Override
    @SneakyThrows
    public void addAll(final String userId, @Nullable final Collection<Task> collection) {
        if (collection == null || collection.isEmpty()) return;
        for (Task item : collection) {
            item.setUserId(userId);
            save(item);
        }
    }

    @Override
    public Task save(final Task entity) {
        if (entity == null) return null;
        repository.save(entity);
        return entity;
    }

    @Nullable
    @Override
    @SneakyThrows
    public Task save(final String userId, @Nullable final Task entity) {
        if (entity == null) return null;
        entity.setUserId(userId);
        @Nullable final Task entityResult = save(entity);
        return entityResult;
    }

    @Override
    public void create(final String userId) {
        save(userId, new Task("new task"));
    }

    @Override
    public void create() {
        repository.save(new Task("new task"));
    }

    @Override
    public Task findById(final String id) {
        final Optional<String> optionalId = Optional.ofNullable(id);
        return repository.findById(optionalId.orElseThrow(EmptyException::new)).get();
    }

    @Nullable
    @Override
    @SneakyThrows
    public Task findById(@NotNull final String userId, @Nullable final String id) {
        @NotNull final Optional<String> optionalId = Optional.ofNullable(id);
        return repository.findByUserIdAndId(userId, optionalId.orElseThrow(EmptyException::new));
    }


    @Override
    public void clear() {
        repository.deleteAll();
    }

    @Override
    @SneakyThrows
    public void clear(@NotNull final String userId) {
        repository.deleteByUserId(userId);
    }

    @Override
    public void removeById(final String id) {
        final Optional<String> optionalId = Optional.ofNullable(id);
        repository.deleteById(optionalId.orElseThrow(EmptyException::new));
    }

    @Override
    @SneakyThrows
    public void removeById(@NotNull final String userId, @Nullable final String id) {
        @NotNull final Optional<String> optionalId = Optional.ofNullable(id);
        repository.deleteByUserIdAndId(userId, optionalId.orElseThrow(EmptyException::new));
    }


    @Override
    public void remove(final Task entity) {
        if (entity == null) return;
        repository.deleteById(entity.getId());
    }
}
