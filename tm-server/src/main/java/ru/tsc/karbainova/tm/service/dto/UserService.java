package ru.tsc.karbainova.tm.service.dto;

import lombok.NonNull;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.tsc.karbainova.tm.api.repository.dto.UserDtoRepository;
import ru.tsc.karbainova.tm.api.service.IPropertyService;
import ru.tsc.karbainova.tm.api.service.dto.IUserService;
import ru.tsc.karbainova.tm.dto.UserDTO;
import ru.tsc.karbainova.tm.enumerated.Role;
import ru.tsc.karbainova.tm.exception.empty.EmptyEmailException;
import ru.tsc.karbainova.tm.exception.empty.EmptyIdException;
import ru.tsc.karbainova.tm.exception.empty.EmptyLoginException;
import ru.tsc.karbainova.tm.exception.empty.EmptyPasswordException;
import ru.tsc.karbainova.tm.exception.empty.EmptyUserNotFoundException;
import ru.tsc.karbainova.tm.exception.entity.ProjectNotFoundException;
import ru.tsc.karbainova.tm.service.PropertyService;
import ru.tsc.karbainova.tm.util.HashUtil;

import java.util.Collection;
import java.util.List;

@Service
public class UserService extends AbstractService<UserDTO> implements IUserService {

    @NotNull
    @Autowired
    private IPropertyService propertyService;

    @Autowired
    private UserDtoRepository userDtoRepository;

    public boolean isLoginExists(@NonNull final String login) {
        if (login.isEmpty()) return false;
        return findByLogin(login) != null;
    }

    @Override
    @SneakyThrows
    public List<UserDTO> findAll() {
        return userDtoRepository.findAll();
    }

    @Override
    @Transactional
    @SneakyThrows
    public void addAll(Collection<UserDTO> collection) {
        if (collection == null) return;
        for (UserDTO i : collection) {
            add(i);
        }
    }

    @Override
    @SneakyThrows
    public UserDTO findByLogin(@NonNull final String login) {
        return userDtoRepository.findFirstByLogin(login);
    }

    @Override
    @SneakyThrows
    public UserDTO findById(@NonNull final String id) {
        return userDtoRepository.findById(id).orElse(null);
    }

    @Override
    @Transactional
    @SneakyThrows
    public UserDTO create(@NonNull final String login, @NonNull final String password) {
        if (login.isEmpty()) throw new EmptyLoginException();
        if (password.isEmpty()) throw new EmptyPasswordException();
        final UserDTO user = new UserDTO();
        user.setRole(Role.USER);
        user.setLogin(login);
        IPropertyService propertyService = new PropertyService();
        user.setPasswordHash(HashUtil.salt(propertyService, password));
        userDtoRepository.save(user);
        return user;
    }

    @Override
    @Transactional
    @SneakyThrows
    public UserDTO create(@NonNull final String login, @NonNull final String password, @NonNull final String email) {
        if (login.isEmpty()) throw new EmptyLoginException();
        if (password.isEmpty()) throw new EmptyPasswordException();
        if (email.isEmpty()) throw new EmptyEmailException();
        if (isLoginExists(login)) throw new EmptyLoginException();
        final UserDTO user = new UserDTO();
        user.setRole(Role.USER);
        user.setLogin(login);
        IPropertyService propertyService = new PropertyService();
        user.setPasswordHash(HashUtil.salt(propertyService, password));
        user.setEmail(email);
        userDtoRepository.save(user);
        return user;
    }

    @Override
    @Transactional
    @SneakyThrows
    public UserDTO setPassword(@NonNull final String userId, @NonNull final String password) {
        if (userId.isEmpty()) throw new EmptyIdException();
        if (password.isEmpty()) throw new EmptyPasswordException();
        IPropertyService propertyService = new PropertyService();
        @Nullable final UserDTO user = userDtoRepository.findById(userId).orElse(null);
        if (user == null) return null;
        final String hash = HashUtil.salt(propertyService, password);
        user.setPasswordHash(hash);
        return user;
    }


    @Override
    @Transactional
    @SneakyThrows
    public UserDTO updateUser(
            @NonNull final String userId,
            @NonNull final String firstName,
            @NonNull final String lastName,
            @Nullable final String middleName) {
        UserDTO user = userDtoRepository.findById(userId).orElse(null);
        if (user == null) throw new ProjectNotFoundException();
        user.setFirstName(firstName);
        user.setLastName(lastName);
        user.setMiddleName(middleName);
        userDtoRepository.save(user);
        return user;
    }

    @Override
    @Transactional
    @SneakyThrows
    public UserDTO add(UserDTO user) {
        if (user == null) throw new EmptyUserNotFoundException();
        userDtoRepository.save(user);
        return user;
    }

    @Override
    @Transactional
    @SneakyThrows
    public void clear() {
        userDtoRepository.deleteAll();
    }
}
