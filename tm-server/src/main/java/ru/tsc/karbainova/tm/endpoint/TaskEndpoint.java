package ru.tsc.karbainova.tm.endpoint;

import lombok.NoArgsConstructor;
import lombok.NonNull;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.tsc.karbainova.tm.api.service.dto.ITaskService;
import ru.tsc.karbainova.tm.dto.SessionDTO;
import ru.tsc.karbainova.tm.dto.TaskDTO;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.List;

@Component
@WebService
@NoArgsConstructor
public class TaskEndpoint extends AbstractEndpoint {

    @NotNull
    @Autowired
    protected ITaskService taskService;

    @WebMethod
    public void clearTask(
            @WebParam(name = "session") final SessionDTO session
    ) {
        sessionService.validate(session);
        taskService.clear();
    }

    @WebMethod
    public void createTaskAllParam(
            @WebParam(name = "session") final SessionDTO session,
            @WebParam(name = "name") @NonNull String name,
            @WebParam(name = "description") @NonNull String description
    ) {
        sessionService.validate(session);
        taskService.create(session.getUserId(), name, description);
    }

    @WebMethod
    public void addTask(
            @WebParam(name = "session") final SessionDTO session,
            @WebParam(name = "task") TaskDTO task
    ) {
        sessionService.validate(session);
        taskService.add(task);
    }

    @WebMethod
    public void removeTask(
            @WebParam(name = "session") final SessionDTO session,
            @WebParam(name = "task") TaskDTO task
    ) {
        sessionService.validate(session);
        taskService.remove(session.getUserId(), task);
    }

    @WebMethod
    public List<TaskDTO> findAllTask(
            @WebParam(name = "session") final SessionDTO session
    ) {
        sessionService.validate(session);
        return taskService.findAll();
    }

    @WebMethod
    public List<TaskDTO> findAllTaskByUserId(
            @WebParam(name = "session") final SessionDTO session
    ) {
        sessionService.validate(session);
        return taskService.findAllTaskByUserId(session.getUserId());
    }

    @WebMethod
    public TaskDTO findByIdTask(
            @WebParam(name = "session") final SessionDTO session,
            @WebParam(name = "id") @NonNull String id
    ) {
        sessionService.validate(session);
        return taskService.findByIdUserId(session.getUserId(), id);
    }

    @WebMethod
    public void addAllTask(
            @WebParam(name = "session") final SessionDTO session,
            @WebParam(name = "tasks") List<TaskDTO> tasks) {
        sessionService.validate(session);
        taskService.addAll(tasks);
    }
}
